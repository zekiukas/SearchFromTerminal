# Search From Terminal

This is a Rust program that searches Google using a filter that restricts the results to a set of valid websites. The program takes a search query as input from the command line, creates a filter using a predefined list of valid websites, and opens the resulting Google search in the default web browser.


## Installation
First you have to install [Rustup](https://rustup.rs/), then you can clone the directory `git clone https://codeberg.org/zekiukas/SearchFromTerminal.git` into a folder of your choosing. After that you can either use the cargo run to use the program (Don't forget to build it first, best to run `cargo build --release`). Another thing you could do is to create a script, but I wouldn't recommend it. What I've done is take the binary file in the Releases tab, and use it with a keybind.
- `cd [folder]`
- `cargo run (index)` index is the input needed for the search.




## Usage

To use the program, run it from the command line and provide a search query as an argument:

- `cargo run <search query>`

For example, to search for "rust web development" on the valid websites, you can run:

- `cargo run rust web development`

This will open a Google (or other search engines) search URL in your default web browser that searches for "rust web development" on the valid websites.

## Valid Websites

The program uses a list of valid websites that restricts the search results to a predefined set of websites. The current list of valid websites is:

- reddit.com
- stackoverflow.com
- stackexchange.com
- medium.com

You can modify this list by changing the `VALID_WEBSITES` constant in the code.

## Dependencies

The program uses the following external dependencies:

- `std::env` (from the Rust standard library) for command line argument parsing
- `webbrowser` crate for opening the search URL in the default web browser

You can use `webbrowser` after adding it to the Cargo.toml file after `[dependencies]`

## License

This program is licensed under the MIT license. See the LICENSE file for details.










































